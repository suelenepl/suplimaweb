'use strict';

angular.module('SuplimaWeb', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',
  'ngStorage',
  'ngLocalize',
  'restangular',
  'ngMask',
  'oitozero.ngSweetAlert',
  'ngMessages',
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.expandable',
  'ui.grid.selection',
  'ui.grid.pinning',
  'ui.grid.exporter',
  'angularMoment',

  'SuplimaWeb.theme',
  'SuplimaWeb.pages'
]).config(function(RestangularProvider){
  //RestangularProvider.setBaseUrl('http://localhost:3000/api');
  RestangularProvider.setBaseUrl('https://suplimaapi.herokuapp.com/api');

}).run(function($localStorage,$state,$rootScope,Restangular){

  $rootScope.$state = $state;

  $rootScope.$storage = $localStorage;

  Restangular.setDefaultHeaders({ token: $localStorage.token });

  Restangular.setErrorInterceptor(function (response, deferred, responseHandler) {

    if (response.status === 403 || response.status === 401) {
      if (response.config.url == "/signin") {
        //  allow processing to continue so my login controller can show an error message
        return true;
      } else {
        $state.go('signin');
        return false;
      }
    }
    return true; // error not handled
  });
  $rootScope.$on('$stateChangeStart',

      function(event, toState, toParams, fromState, fromParams){

        if($rootScope.$storage.token == undefined || $rootScope.$storage.token == ''){

          if(!toState.allowAnonymous){

            event.preventDefault();

            $state.go("signin", {});
          }
        }
      }
  );
});