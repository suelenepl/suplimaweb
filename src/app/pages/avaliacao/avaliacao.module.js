/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('SuplimaWeb.pages.avaliacao', [])
      .config(routeConfig).run(function($rootScope,$state){
             $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if(toState.name=="avaliacao"){
                        event.preventDefault();
                        $state.go("avaliacao.main", {});
                    }
                }
            );
        });

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
            .state('avaliacao', {
                url: '/avaliacao',
                templateUrl: 'app/pages/shared/content.html',
                title:'Avaliações',
                sidebarMeta: {
                    order: 200,
                    icon: 'ion-document',
                }
            })
            .state('avaliacao.main', {
                url: '/main',
                title: 'Avaliações',
                templateUrl: 'app/pages/avaliacao/avaliacao.html',
                controller: 'AvaliacaoPageCtrl'
            });
  }

})();
