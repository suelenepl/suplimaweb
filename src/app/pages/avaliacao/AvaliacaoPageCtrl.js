/**
 * Created by guilherme on 08/09/16.
 */
(function () {
    'use strict';

    angular.module('SuplimaWeb.pages.avaliacao')
        .controller('AvaliacaoPageCtrl', AvaliacaoPageCtrl);

    /** @ngInject */
    function AvaliacaoPageCtrl($scope , Restangular, SweetAlert,$state,$localStorage) {
        $scope.showCadastro = false;
        $scope.showLista = true;

        var loadData = function(){
            $scope.cliente = {};
            $scope.Avaliacao = {};
            Restangular.one('users').get().then(function(res){
                $scope.gridClientes.data = res.users;
            })
        }
        loadData();

        $scope.enableCadastro = function(){
            $scope.showCadastro = !$scope.showCadastro;
        }

        $scope.cancel = function(){
            $scope.showCadastro = !$scope.showCadastro;
        }

        $scope.gridcopeProvider = {

            loadCliente: function (row) {
                $scope.cliente = row.entity;
                $scope.showCadastro = !$scope.showCadastro;
                _.defer(function () {
                    $scope.$apply();
                });
            }
        };
        $scope.gridClientes = {
            enableFiltering: true,
            enableRowSelection: true,
            enableGridMenu: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            columnDefs: [
                { field: '_id', visible: false },
                { field: 'fullName', name: 'Nome' },
                { field: 'age', name: 'Idade' },
                { field: 'phones', name: 'Tel.' },
                { field: 'emails', name: 'E-mail' },
                { field: 'createdAt', name:'Data de Cadastro'}
            ],
            appScopeProvider: $scope.gridcopeProvider,
            rowTemplate: "<div ng-click=\"grid.appScope.loadAvaliacao(row)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>"
        };
    }

})();