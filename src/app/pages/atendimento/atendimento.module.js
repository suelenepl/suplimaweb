/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('SuplimaWeb.pages.atendimento', [])
      .config(routeConfig).run(function($rootScope,$state){
             $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if(toState.name=="atendimento"){
                        event.preventDefault();
                        $state.go("atendimento.main", {});
                    }
                }
            );
        });

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
            .state('atendimento', {
                url: '/atendimento',
                templateUrl: 'app/pages/shared/content.html',
                title:'Atendimentos',
                sidebarMeta: {
                    order: 200,
                    icon: 'ion-document',
                }
            })
            .state('atendimento.main', {
                url: '/main',
                title: 'Atendimentos',
                templateUrl: 'app/pages/atendimento/atendimento.html',
                controller: 'AtendimentoPageCtrl'
            });
  }

})();
