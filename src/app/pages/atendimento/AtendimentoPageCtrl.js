/**
 * Created by guilherme on 08/09/16.
 */
(function () {
    'use strict';

    angular.module('SuplimaWeb.pages.atendimento')
        .controller('AtendimentoPageCtrl', AtendimentoPageCtrl);

    /** @ngInject */
    function AtendimentoPageCtrl($scope , Restangular, SweetAlert,$state,$localStorage) {
        $scope.showCadastro = false;
        $scope.showLista = true;
        var loadData = function(){
            $scope.cliente = {};
            $scope.atendimento = {};
            Restangular.one('users').get().then(function(res){
                $scope.gridClientes.data = res.users;
            })
        }
        var loadAtendimentos = function(){
            $scope.gridAtendimento.data = [];
            if($scope.cliente._id){
                Restangular.one('atendimentos').one('cliente',$scope.cliente._id).get().then(function(res){
                    _.forEach(res.atendimentos,function(atendimento){
                        if(typeof atendimento.sinVitais === "string"){
                            atendimento.sinVitais = JSON.parse(atendimento.sinVitais);
                        }
                    })
                    $scope.gridAtendimento.data = res.atendimentos;
                });
            }
        }
        loadData();
        $scope.enableCadastro = function(){
            if($scope.cliente._id){
                $scope.showCadastro = true;
                $scope.atendimento = {};
            }else{
                SweetAlert.swal("Ops!", "Selecione um cliente!", "error");
            }
            
        }
        $scope.cancel = function(){
            $scope.atendimento = {};
            $scope.showCadastro = !$scope.showCadastro;
        }
        $scope.save = function(atendimentoForm){
            atendimentoForm.$submitted = true;
            if (atendimentoForm.$valid) {
                if(!$scope.atendimento._id){
                    $scope.atendimento.cliente = $scope.cliente._id;
                    Restangular.one('atendimentos').post('',$scope.atendimento).then(function(res){
                        $scope.showCadastro = !$scope.showCadastro;
                        loadData();
                        loadAtendimentos();
                        _.defer(function(){
                            $scope.$apply();
                        })
                    });
                }else{
                    delete $scope.atendimento.$$hashKey;
                    Restangular.one('atendimentos',$scope.atendimento._id).put($scope.atendimento).then(function(res){
                        $scope.showCadastro = !$scope.showCadastro;
                        loadData();
                        loadAtendimentos();
                        _.defer(function(){
                            $scope.$apply();
                        })
                    });
                }
            }
        }

        $scope.gridcopeProvider = {

            loadAtendimento: function (row) {
                row.entity.dataAtend = new Date(row.entity.dataAtend);
                $scope.atendimento = row.entity;
                $scope.showCadastro = true;
                $scope.atendimento.cliente = $scope.cliente._id;
                if(typeof row.entity.sinVitais === "string"){
                    row.entity.sinVitais = JSON.parse(row.entity.sinVitais);
                }
                if(typeof row.entity.medicao === "string"){
                    row.entity.medicao = JSON.parse(row.entity.medicao);
                }

                _.defer(function () {
                    $scope.$apply();
                });
            },
            loadCliente: function (row) {
                $scope.cliente = row.entity;
                loadAtendimentos();
                
                _.defer(function () {
                    $scope.$apply();
                });
            }
        };
        $scope.gridClientes = {
            enableFiltering: true,
            enableRowSelection: true,
            enableGridMenu: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            columnDefs: [
                { field: '_id', visible: false },
                { field: 'fullName', name: 'Cliente'}
            ],
            appScopeProvider: $scope.gridcopeProvider,
            rowTemplate: "<div ng-click=\"grid.appScope.loadCliente(row)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>"
        };
        $scope.gridAtendimento = {
            enableFiltering: true,
            enableRowSelection: true,
            enableGridMenu: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            columnDefs: [
                { field: '_id', visible: false },
                { field: 'dataAtend', name: 'Data do Atendimento'},
                { field:'profissional', name:'Profissional'}
            ],
            appScopeProvider: $scope.gridcopeProvider,
            rowTemplate: "<div ng-click=\"grid.appScope.loadAtendimento(row)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>"
        };
    }

})();