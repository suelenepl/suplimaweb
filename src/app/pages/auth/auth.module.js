/**
 * Created by guilherme on 08/09/16.
 */
/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('SuplimaWeb.pages.auth', [
        'ngLocalize.Config'
    ])
        .config(routeConfig)
        .value('localeConf', {
            basePath: 'app/pages/auth/languages',
            defaultLocale: 'pt-BR',
            fileExtension: '.lang.json'
        });

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('signin', {
                url: '/signin',
                title: 'Bem Vindo',
                templateUrl: 'app/pages/auth/signin/signin.html',
                controller: 'SigninPageCtrl',
                allowAnonymous:true
            })
            .state('register', {
                url: '/register',
                title: 'Bem Vindo',
                templateUrl: 'app/pages/auth/register/register.html',
                controller: 'RegisterPageCtrl',
                allowAnonymous:true
            });
    }

})();
