/**
 * Created by guilherme on 08/09/16.
 */
(function () {
    'use strict';

    angular.module('SuplimaWeb.pages.auth')
        .controller('SigninPageCtrl', SigninPageCtrl);

    /** @ngInject */
    function SigninPageCtrl($scope,$rootScope , Restangular, SweetAlert,$state,$localStorage) {
        $scope.signin = function(signinForm) {
            signinForm.$submitted = true;
            Restangular.setDefaultHeaders({token: undefined});
            if (signinForm.$valid) {
                if ($scope.user.username === 'admin' && $scope.user.password === 'admin'){
                     $localStorage.token = 'campo de token novo modulo';
                     $localStorage.user = 'admin';
                     Restangular.setDefaultHeaders({token: $localStorage.token});
                     $state.go('dashboard.main');
                }
            }
        };
    }

})();