/**
 * Created by guilherme on 08/09/16.
 */
/**
 * Created by guilherme on 08/09/16.
 */
(function () {
    'use strict';

    angular.module('SuplimaWeb.pages.auth')
        .controller('RegisterPageCtrl', RegisterPageCtrl);

    /** @ngInject */
    function RegisterPageCtrl($scope,Restangular,SweetAlert,locale,$state,$localStorage) {
        $scope.user = {};
        $scope.user.address = {};
        var loadData = function(){
            Restangular.one('locates').one('states').get().then(function(res){
                $scope.states = res.states;
            });
        }
        loadData();
        $scope.loadCities = function(){
            if($scope.selectedState){
                $scope.user.address.state = $scope.selectedState.shortname;
                Restangular.one('locates').one('cities',$scope.selectedState._id).get().then(function(res){
                    $scope.cities = res.cities;
                });
            }else{
                $scope.cities = [];
            }
        }
        locale.ready('errors').then(function () {
            $scope.errorMessages = {};
            $scope.errorMessages.title = locale.getString('errors.title');
            $scope.errorMessages.e400 = locale.getString('errors.400')
        });
        $scope.save = function(registerForm) {
            registerForm.$submitted = true;
            if (registerForm.$valid) {
                Restangular.one('auth').post('signup', $scope.user).then(function (res) {
                    $localStorage.token = res.token;
                    SweetAlert.swal({
                            title: "Bem vindo ao Suplima",
                            text: "Cadastro realizado com sucesso",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Continuar",
                            closeOnConfirm: true
                        },
                        function () {
                            Restangular.setDefaultHeaders({token: $localStorage.token});
                            $state.go('dashboard.main');
                        });
                }, function (err) {
                    if (err.status === 400)
                        SweetAlert.swal($scope.errorMessages.title, $scope.errorMessages.e400 + ' ' + err.data.message, "error");
                });
            }
        };
    }

})();