/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('SuplimaWeb.pages', [
    'ui.router',

    'SuplimaWeb.pages.dashboard',
    'SuplimaWeb.pages.auth',
    'SuplimaWeb.pages.cliente',
    //'SuplimaWeb.pages.avaliacao',
    'SuplimaWeb.pages.atendimento'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/home/main');

    
  
  }

})();
