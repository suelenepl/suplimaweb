/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('SuplimaWeb.pages.cliente', [])
      .config(routeConfig).run(function($rootScope,$state){
             $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if(toState.name=="cliente"){
                        event.preventDefault();
                        $state.go("cliente.main", {});
                    }
                }
            );
        });

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
            .state('cliente', {
                url: '/cliente',
                templateUrl: 'app/pages/shared/content.html',
                title:'Clientes',
                sidebarMeta: {
                    order: 100,
                    icon: 'ion-document',
                }
            })
            .state('cliente.main', {
                url: '/main',
                title: 'Clientes',
                templateUrl: 'app/pages/cliente/cliente.html',
                controller: 'ClientePageCtrl'
            });
  }

})();
