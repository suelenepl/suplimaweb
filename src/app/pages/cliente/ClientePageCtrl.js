/**
 * Created by guilherme on 08/09/16.
 */
(function () {
    'use strict';

    angular.module('SuplimaWeb.pages.cliente')
        .controller('ClientePageCtrl', ClientePageCtrl);

    /** @ngInject */
    function ClientePageCtrl($scope , Restangular, SweetAlert,$state,$localStorage) {
        $scope.showCadastro = false;
        

        var loadData = function(){
            $scope.usuario = {};
            $scope.usuario.doc = {};
            $scope.usuario.address = {};
            Restangular.one('users').get().then(function(res){
                $scope.gridClientes.data = res.users;
            })
        }
        loadData();
        $scope.showLista = true;

        $scope.enableCadastro = function(){
            $scope.showCadastro = !$scope.showCadastro;
            loadData();
            _.defer(function(){
                $scope.$apply();
            });
        }
        $scope.cancel = function(){
            $scope.showCadastro = !$scope.showCadastro;
        }

        $scope.save = function(clienteForm){
            clienteForm.$submitted = true;
            if (clienteForm.$valid) {
                if(!$scope.usuario._id){
                    Restangular.one('users').post('',$scope.usuario).then(function(res){
                        $scope.showCadastro = !$scope.showCadastro;
                        loadData();
                        _.defer(function(){
                            $scope.$apply();
                        })
                    });
                }else{
                    delete $scope.usuario.$$hashKey;
                    Restangular.one('users',$scope.usuario._id).put($scope.usuario).then(function(res){
                        $scope.showCadastro = !$scope.showCadastro;
                        loadData();
                        _.defer(function(){
                            $scope.$apply();
                        })
                    });
                }
            }
        }

        $scope.gridcopeProvider = {

            loadCliente: function (row) {
                if(typeof row.entity.doc === "string"){
                    row.entity.doc = JSON.parse(row.entity.doc);
                }
                if(typeof row.entity.address === "string"){
                    row.entity.address = JSON.parse(row.entity.address);
                }
                if(row.entity.doc.dataExp){
                    row.entity.doc.dataExp = new Date(row.entity.doc.dataExp);
                    row.entity.doc.birth.date = new Date(row.entity.doc.birth.date);
                }
                $scope.usuario = row.entity;
                $scope.showCadastro = !$scope.showCadastro;
                _.defer(function () {
                    $scope.$apply();
                });
            }
        };
        $scope.gridClientes = {
            enableFiltering: true,
            enableRowSelection: true,
            enableGridMenu: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            columnDefs: [
                { field: '_id', visible: false },
                { field: 'fullName', name: 'Nome' },
                { field: 'age', name: 'Idade' },
                { field: 'phones', name: 'Tel.' },
                { field: 'emails', name: 'E-mail' },
            ],
            appScopeProvider: $scope.gridcopeProvider,
            rowTemplate: "<div ng-click=\"grid.appScope.loadCliente(row)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>"
        };
    }

})();