/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('SuplimaWeb.pages.dashboard', [])
      .config(routeConfig).run(function($rootScope,$state){
             $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if(toState.name=="dashboard"){
                        event.preventDefault();
                        $state.go("dashboard.main", {});
                    }
                }
            );
        });

  /** @ngInject */
  function routeConfig($stateProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/home',
                templateUrl: 'app/pages/shared/content.html',
                title:'Home',
                sidebarMeta: {
                    order: 0,
                    icon: 'ion-document',
                }
            })
            .state('dashboard.main', {
                url: '/main',
                title: 'Principal',
                templateUrl: 'app/pages/dashboard/dashboard.html',
                controller: 'DashboardPageCtrl'
            });
  }

})();
